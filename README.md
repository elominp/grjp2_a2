# Where are my friends Android application

This little application grabs the position of your friends from a
webservice and show them on a map.

The source code and its history can be seen on Gitlab: https://gitlab.com/elominp1/grjp2_a2

The documentation can be read on this website: https://elominp1.gitlab.io/grjp2_a2/index.html

## How to use it?

It's very easy:

- Download the app by downloading it from this link: https://gitlab.com/elominp1/grjp2_a2/-/jobs/artifacts/master/download?job=deploy
- Ensure you have allowed to install applications from unknown sources
- Open the downloaded grjp2_a2.apk file from any file manager on your device and accept to install it
- Open the app
- Enjoy!

## Bonus features

- Stability ensured by unit tests of functionalities -> See UnitTest classes documentation
- Continuous integration of the application by the building of the application, execution of tests
and deployment triggered each time a new commit is pushed on the remote repository
- Automatic generation of this documentation website + automatic deployment triggered when new
commits are pushed on the remote repository
- Automatic refresh of friends positions
- Possibility to use an extended web API by extending the CO838ApiClient class -> See the
CO838ApiExtendedClient for example
- Side drawer with user image and pseudonyme if authenticated + switch to use extended webservice (back-end not implemented yet)

## Dependencies:

- External libraries
  + Google GSON library for JSON native deserializing in Java classes
  + Retrofit library for REST API (and classic HTTP) requests
  + Drawer background from Oxygenna (http://files.oxygenna.com/material-bgs-3.zip)
  + Icon from freeiconshop (http://freeiconshop.com/icon/location-map-icon-flat/)