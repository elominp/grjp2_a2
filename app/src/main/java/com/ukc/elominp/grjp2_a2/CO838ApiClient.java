package com.ukc.elominp.grjp2_a2;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Interface for the base API defined by the IoT assessment
 */
public interface CO838ApiClient {
    @GET("/people/staff/iau/LocalUsers.php")
    Call<IoTKentUsers> localUsers();
}
