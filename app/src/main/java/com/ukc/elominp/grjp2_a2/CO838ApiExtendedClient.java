package com.ukc.elominp.grjp2_a2;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Enable adding new endpoints to the base API of the IoT assignment
 */
public interface CO838ApiExtendedClient extends CO838ApiClient {
    @GET("/users/{user}")
    Call<IoTKentUsers.IoTKentUser> user(@Path("user") String user);
}
