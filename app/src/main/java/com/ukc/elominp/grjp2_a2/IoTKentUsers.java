package com.ukc.elominp.grjp2_a2;

import java.util.List;

/**
 * Class used to represent the JSON data used by the /LocalUsers.php endpoint
 */
public class IoTKentUsers {
    /**
     * Nested class representing an element of the "Users" nested list of the JSON
     * used by the /LocalUsers.php endpoint
     */
    public class IoTKentUser {
        private String  name;
        private String  lon;
        private String  lat;

        public          IoTKentUser() {}

        public String   getName() { return name; }
        public float    getLongitude() { return Float.parseFloat(lon); }
        public float    getLatitude() { return Float.parseFloat(lat); }
    }

    /**
     * "Users" element of the JSON return by /LocalUsers.php
     */
    private List<IoTKentUser> Users;

    public                      IoTKentUsers() {}

    public List<IoTKentUser>    getUsers() { return Users; }
}
