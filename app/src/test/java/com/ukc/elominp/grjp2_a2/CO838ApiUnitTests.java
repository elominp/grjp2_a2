package com.ukc.elominp.grjp2_a2;

import org.junit.Test;
import static org.junit.Assert.*;

import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class implementing unit tests of the base API for the mandatory part of the assignment
 */
public class CO838ApiUnitTests {
    private String      _api_base_url = "https://www.cs.kent.ac.uk/";

    /**
     * Test the unique request type possible for the base API to retrieve positions of users.
     *
     * Execute and expect the request to works and parse the JSON obtained in response,
     * expecting it to contains valid data.
     * @throws Exception
     */
    @Test
    public void localUsers() throws Exception {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(_api_base_url)
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.client(httpBuilder.build()).build();
        CO838ApiClient client = retrofit.create(CO838ApiClient.class);

        Response<IoTKentUsers> response = client.localUsers().execute();
        if (response.isSuccessful()) {
            IoTKentUsers users = response.body();

            /*
            Java8 forEach invocation needs Android API level 24 minimum
            Unfortunately, my android device with the most recent android version has
            only Android 5.0 (API level 21), so I can't use this nice feature :(
            */
            /*
            Should we assume the answer of the endpoint is not static and thus not hardcoding
            the results but just checking there's values?
             */
            for (IoTKentUsers.IoTKentUser user : users.getUsers()) {
                assertNotNull(user.getName());
                assertNotEquals("", user.getName());
                assertNotEquals(0., user.getLatitude());
                assertNotEquals(0, user.getLongitude());
            }
        } else {
            throw new RuntimeException(
                    "Server responded to users location requests by an error: " +
                            Integer.toString(response.code())
            );
        }
    }
}
